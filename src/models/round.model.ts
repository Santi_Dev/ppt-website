import { PlayerMove } from "./playerMove.model";

export interface Round {
    gameId: string;
    playerMoves: Array<PlayerMove>
}
