export interface Player {
    id: string;
    gameId: string;
    name: string;
}