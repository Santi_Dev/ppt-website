import { Player } from "./player.model";
import { Round } from "./round.model";

export interface Game {
    id: string;
    enabled: boolean;
    players: Array<Player>;
    rounds: Array<Round>
}
