import { BaseResponse } from "./baseResponse.model";

export class GeneralResponse<T> extends BaseResponse {
    item: T;
    list: Array<T>
}