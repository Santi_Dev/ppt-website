import { MessageCode } from "./enums/messageCode.enum";

export class BaseResponse {
    status: MessageCode;
    message: string;
}