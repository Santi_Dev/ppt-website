import { Component, Input, OnInit } from "@angular/core";
import { AbstractControl, FormControl } from "@angular/forms";

@Component({
    selector: 'app-show-error-component',
    templateUrl: './show-error.component.html'
})
export class ShowErrorComponent implements OnInit {

    @Input()
    control: FormControl | AbstractControl;

    messages = {
        required: 'Este campo es requerido',
    };

    messageErrors: string[] = [];

    ngOnInit() {
        this.control.statusChanges.subscribe(() => {
            if (this.control.invalid) {
                const errors = Object.keys(this.control.errors);
                this.messageErrors = [];
                for (const error in errors) {
                    if (error) {
                        const key = errors[error];
                        this.messageErrors.push(this.messages[key]);
                    }
                }
            }
        });
    }

    get hasError() {
        return (
            this.control &&
            (this.control.touched || this.control.dirty) &&
            this.control.invalid
        );
    }
}
