import { Directive, ElementRef, Input, OnInit, Renderer2 } from "@angular/core";
import { AbstractControl, FormControl } from "@angular/forms";

@Directive({
    selector: '[appError]'
})
export class ShowErrorDirective implements OnInit {

    @Input('control')
    control: FormControl | AbstractControl;

    constructor(private render: Renderer2, private el: ElementRef) { }

    ngOnInit() {
        this.control.statusChanges.subscribe(() => {
            if (this.control && (this.control.touched || this.control.dirty) && this.control.invalid) {
                this.render.addClass(this.el.nativeElement, 'is-invalid');
            } else {
                this.render.removeClass(this.el.nativeElement, 'is-invalid');
            }
        });
    }
}