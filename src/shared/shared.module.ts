import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { FooterComponent } from "./components/footer/footer.component";
import { HeaderComponent } from "./components/header/header.component";
import { ShowErrorComponent } from "./components/show-error/show-error.component";
import { ShowErrorDirective } from "./directives/show-error/show-error.directive";

@NgModule({
    declarations: [
        ShowErrorComponent,
        ShowErrorDirective,
        FooterComponent,
        HeaderComponent
    ],
    imports: [
        ReactiveFormsModule,
        CommonModule
    ],
    exports: [
        ShowErrorComponent,
        ShowErrorDirective,
        FooterComponent,
        HeaderComponent
    ],
    providers: []
})
export default class SharedModule { }
