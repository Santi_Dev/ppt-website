import { environment } from "src/environments/environment";

export default class BaseService {
    protected apiUrl = environment.apiUrl;
}