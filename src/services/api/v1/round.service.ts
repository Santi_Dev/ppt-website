import { Injectable } from "@angular/core";
import BaseService from "../base.service";
import { HttpClient } from '@angular/common/http';
import { Round } from "../../../models/round.model";
import { BaseResponse } from "../../../models/baseResponse.model";

@Injectable()
export default class RoundService extends BaseService {
    private serviceUrl = `${this.apiUrl}/v1/rounds`;

    constructor(private rest: HttpClient) {
        super();
    }

    addRound(round: Round) {
        return this.rest.post<BaseResponse>(this.serviceUrl, round);
    }
}