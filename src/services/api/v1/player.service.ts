import { Injectable } from "@angular/core";
import BaseService from "../base.service";
import { HttpClient } from '@angular/common/http';
import { GeneralResponse } from "../../../models/generalResponse.model";
import { Player } from "../../../models/player.model";

@Injectable()
export default class PlayerService extends BaseService {
    private serviceUrl = `${this.apiUrl}/v1/players`;

    constructor(private rest: HttpClient) {
        super();
    }

    getWinner(gameId: string) {
        return this.rest.get<GeneralResponse<Player>>(`${this.serviceUrl}/games/${gameId}/winner`);
    }
}