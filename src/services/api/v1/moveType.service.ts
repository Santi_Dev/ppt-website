import { Injectable } from "@angular/core";
import BaseService from "../base.service";
import { HttpClient } from '@angular/common/http';
import { GeneralResponse } from "../../../models/generalResponse.model";
import { MoveType } from "../../../models/moveType.model";

@Injectable()
export default class MoveTypeService extends BaseService {
    private serviceUrl = `${this.apiUrl}/v1/moveTypes`;

    constructor(private rest: HttpClient) {
        super();
    }

    getMoveTypes() {
        return this.rest.get<GeneralResponse<MoveType>>(this.serviceUrl);
    }
}