import { Injectable } from "@angular/core";
import BaseService from "../base.service";
import { HttpClient } from '@angular/common/http';
import { Game } from "../../../models/game.model";
import { BaseResponse } from "../../../models/baseResponse.model";
import { GeneralResponse } from "../../../models/generalResponse.model";

@Injectable()
export default class GameService extends BaseService {
    private serviceUrl = `${this.apiUrl}/v1/games`;

    constructor(private rest: HttpClient) {
        super();
    }

    addGame(newGame: Game) {
        return this.rest.post<GeneralResponse<Game>>(this.serviceUrl, newGame);
    }

    resetGame(gameId: string) {
        return this.rest.put<BaseResponse>(`${this.serviceUrl}/${gameId}/reset`, {});
    }

    getGameById(gameId: string) {
        return this.rest.get<GeneralResponse<Game>>(`${this.serviceUrl}/${gameId}`);
    }
}