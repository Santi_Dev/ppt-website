import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import SharedModule from '../shared/shared.module';
import { NgxUiLoaderModule, NgxUiLoaderRouterModule } from 'ngx-ui-loader';
import { ToastrModule } from 'ngx-toastr';
import { NgWizardModule, THEME } from 'ng-wizard';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    BrowserAnimationsModule,
    NgxUiLoaderModule,
    NgxUiLoaderRouterModule,
    ToastrModule.forRoot(),
    NgWizardModule.forRoot({
      theme: THEME.arrows
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
