import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import AppRoutes from '../constants/routes';

const routes: Routes = [
  {
    path: "",
    redirectTo: `${AppRoutes.GAME_ROUTE}/new`,
    pathMatch: "full"
  },
  {
    path: '',
    loadChildren: () => import("./pages/pages.module").then(m => m.PagesModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
