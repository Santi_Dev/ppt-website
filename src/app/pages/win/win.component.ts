import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import AppRoutes from 'src/constants/routes';
import GameService from 'src/services/api/v1/game.service';
import { MessageCode } from '../../../models/enums/messageCode.enum';
import { Player } from '../../../models/player.model';
import PlayerService from '../../../services/api/v1/player.service';

@Component({
    selector: 'app-win-component',
    templateUrl: './win.component.html'
})
export class WinComponent implements OnInit {

    winnerInfo: Player;

    constructor(private activedRouter: ActivatedRoute,
        private playerService: PlayerService,
        private gameService: GameService,
        private toastr: ToastrService,
        private router: Router) { }

    ngOnInit() {
        const gameId = this.activedRouter.snapshot.params.id;
        this.playerService.getWinner(gameId).subscribe(res => {
            if (res.status === MessageCode.Success) {
                this.winnerInfo = res.item;
                return;
            }

            this.toastr.error(res.message);
        }, (err: HttpErrorResponse) => {
            const errorMessage = err.error.message || err.message;
            this.toastr.error(errorMessage);
        });
    }

    rematch() {
        this.gameService.resetGame(this.winnerInfo.gameId).subscribe(res => {
            if (res.status === MessageCode.Success) {
                this.router.navigate([`/${AppRoutes.GAME_ROUTE}/${this.winnerInfo.gameId}/rounds`]);
                return;
            }

            this.toastr.error(res.message);
        }, (err: HttpErrorResponse) => {
            const errorMessage = err.error.message || err.message;
            this.toastr.error(errorMessage);
        })
    }

    newGame() {
        this.router.navigate([`/${AppRoutes.GAME_ROUTE}/new`]);
    }

}
