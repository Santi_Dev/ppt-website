import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from '../app.component';
import GameComponent from '../pages/game/game.component';
import GameService from '../../services/api/v1/game.service';
import { ReactiveFormsModule } from '@angular/forms';
import RoundComponent from '../pages/round/round.component';
import SharedModule from '../../shared/shared.module';
import PlayerGameComponent from '../pages/game/player/player-game.component';
import { NgxUiLoaderHttpModule } from 'ngx-ui-loader';
import MoveTypeService from '../../services/api/v1/moveType.service';
import PlayerRoundComponent from '../pages/round/player/player-round.component';
import ScoreboardComponent from '../pages/round/scoreboard/scoreboard.component';
import { NgWizardModule, THEME } from 'ng-wizard';
import { WinComponent } from '../pages/win/win.component';
import RoundService from '../../services/api/v1/round.service';
import PlayerService from '../../services/api/v1/player.service';
import { PagesRoutingModule } from './pages-rounting.module';
import { PageComponent } from './pages.component';
import { CommonModule } from '@angular/common';

@NgModule({
    declarations: [
        PageComponent,
        GameComponent,
        RoundComponent,
        PlayerGameComponent,
        PlayerRoundComponent,
        ScoreboardComponent,
        WinComponent
    ],
    imports: [
        CommonModule,
        HttpClientModule,
        ReactiveFormsModule,
        SharedModule,
        NgxUiLoaderHttpModule.forRoot({
            showForeground: true
        }),
        NgWizardModule.forRoot({
            theme: THEME.arrows
        }),
        PagesRoutingModule
    ],
    providers: [
        GameService,
        MoveTypeService,
        RoundService,
        PlayerService
    ],
    bootstrap: [AppComponent]
})
export class PagesModule { }
