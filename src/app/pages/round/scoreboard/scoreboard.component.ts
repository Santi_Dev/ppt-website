import { Component, Input } from "@angular/core";
import { Round } from "src/models/round.model";

@Component({
    selector: "app-scoreboard-component",
    templateUrl: "./scoreboard.component.html"
})
export default class ScoreboardComponent { 

    @Input()
    rounds: Array<Round>
}
