import { HttpErrorResponse } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { NgWizardConfig, NgWizardService } from "ng-wizard";
import { ToastrService } from "ngx-toastr";
import { MessageCode } from "../../../models/enums/messageCode.enum";
import { PlayerMove } from "../../../models/playerMove.model";
import { Round } from "../../../models/round.model";
import RoundService from "../../../services/api/v1/round.service";
import { Game } from "../../../models/game.model";
import GameService from "../../../services/api/v1/game.service";
import AppRoutes from "src/constants/routes";

@Component({
    selector: "app-round-component",
    templateUrl: "./round.component.html"
})
export default class RoundComponent implements OnInit {

    currentGame: Game;
    currentRound: Round;

    config: NgWizardConfig = {
        selected: 0,
        anchorSettings: {
            anchorClickable: false
        },
        toolbarSettings: {
            showNextButton: false,
            showPreviousButton: false
        }
    };

    constructor(private activedRouter: ActivatedRoute,
        private gameService: GameService,
        private toasts: ToastrService,
        private ngWizardService: NgWizardService,
        private roundService: RoundService,
        private router: Router) { }

    ngOnInit() {
        const gameId = this.activedRouter.snapshot.params.id;
        this.validateGameStatus(gameId);
        this.currentRound = {
            gameId: gameId,
            playerMoves: []
        }
    }

    handlePlayerMove(playerMove: PlayerMove) {
        this.currentRound.playerMoves.push(playerMove);
        if (this.currentGame.players.length === this.currentRound.playerMoves.length) {
            this.roundService.addRound(this.currentRound).subscribe(res => {
                if (res.status === MessageCode.Success) {
                    this.validateGameStatus(this.currentGame.id);
                    return;
                }

                this.toasts.error(res.message);

            }, (err: HttpErrorResponse) => {
                const errorMessage = err.error.message || err.message;
                this.toasts.error(errorMessage);
            });
            return;
        }
        this.ngWizardService.next();
    }

    private validateGameStatus(gameId: string) {
        this.gameService.getGameById(gameId).subscribe(res => {
            if (res.status === MessageCode.Success) {
                if (res.item.enabled) {
                    this.currentGame = res.item;
                    this.resetGame();
                    return;
                }

                // game over
                this.router.navigate([`/${AppRoutes.GAME_ROUTE}/${gameId}/win`]);
                return;
            }

            // send to new game
            this.toasts.error(res.message);
            this.router.navigate([`/${AppRoutes.GAME_ROUTE}/new`]);
            return;

        }, (err: HttpErrorResponse) => {
            const errorMessage = err.error.message || err.message;
            this.toasts.error(errorMessage);
            this.router.navigate([`/${AppRoutes.GAME_ROUTE}/new`]);
        })
    }

    private resetGame() {
        this.ngWizardService.reset();
        this.currentRound.playerMoves = [];
    }
}
