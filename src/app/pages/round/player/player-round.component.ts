import { HttpErrorResponse } from "@angular/common/http";
import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { ToastrService } from "ngx-toastr";
import { MessageCode } from "src/models/enums/messageCode.enum";
import { MoveType } from "src/models/moveType.model";
import { PlayerMove } from "src/models/playerMove.model";
import MoveTypeService from "src/services/api/v1/moveType.service";
import { Player } from "../../../../models/player.model";

@Component({
    selector: "app-player-round-component",
    templateUrl: "./player-round.component.html"
})
export default class PlayerRoundComponent implements OnInit {

    @Input("player")
    playerInfo: Player

    @Output()
    move: EventEmitter<PlayerMove>

    moveTypes: Array<MoveType> = [];
    playerMoveForm: FormGroup;

    constructor(private moveTypeService: MoveTypeService,
        private toastr: ToastrService,
        private fb: FormBuilder) {
        this.move = new EventEmitter();
    }

    ngOnInit() {
        this.getMoveTypes();
        this.playerMoveForm = this.fb.group({
            moveTypeId: new FormControl("", [Validators.required])
        });
    }

    handleBtnNext() {
        const moveTypeId = this.playerMoveForm.value.moveTypeId;
        if (moveTypeId) {
            const playerMove = {
                moveTypeId: this.playerMoveForm.value.moveTypeId,
                playerId: this.playerInfo.id
            }
            this.move.emit(playerMove);
            this.playerMoveForm.reset();
        }
    }

    private getMoveTypes() {
        this.moveTypeService.getMoveTypes().subscribe(res => {
            if (res.status === MessageCode.Success) {
                this.moveTypes = res.list;
                return;
            }

            this.toastr.error(res.message);
        }, (err: HttpErrorResponse) => {
            const errorMessage = err.error.message || err.message;
            this.toastr.error(errorMessage);
        });
    }

    get moveType() {
        return this.playerMoveForm.get("moveTypeId");
    }
}
