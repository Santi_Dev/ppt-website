import { Component, OnInit } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import GameService from "../../../services/api/v1/game.service";
import { Game } from "../../../models/game.model";
import { Router } from "@angular/router";
import { HttpErrorResponse } from "@angular/common/http";
import { MessageCode } from "../../../models/enums/messageCode.enum";
import AppRoutes from "src/constants/routes";
import { ToastrService } from "ngx-toastr";

@Component({
    selector: "app-game-component",
    templateUrl: "./game.component.html"
})
export default class GameComponent implements OnInit {

    gameForm: FormGroup;

    constructor(private fb: FormBuilder, 
                private gameService: GameService, 
                private router: Router,
                private toastr: ToastrService) { }

    ngOnInit() {
        this.gameForm = this.fb.group({
            players: this.fb.array([
                this.fb.group({
                    name: new FormControl("", [Validators.required])
                }),
                this.fb.group({
                    name: new FormControl("", [Validators.required])
                }),
            ])
        })
    }

    start() {
        const newGame = this.gameForm.value as Game;
        this.gameService.addGame(newGame).subscribe(res => {
            
            if(res.status === MessageCode.Success) {
                this.router.navigate([`/${AppRoutes.GAME_ROUTE}/${res.item.id}/rounds`]);
                return;
            }

            this.toastr.error(res.message);

        }, (err: HttpErrorResponse) => {
            const errorMessage = err.error.message || err.message;
            this.toastr.error(errorMessage);
        });
    }

    get players() {
        return this.gameForm.get('players') as FormArray;
    }
}
