import { Component, Input } from "@angular/core";
import { FormControl } from "@angular/forms";

@Component({
    selector: "app-player-game-component",
    templateUrl: "./player-game.component.html"
})
export default class PlayerComponent { 

    @Input()
    index: number;

    @Input()
    control: FormControl;

}
