import { Component } from '@angular/core';

@Component({
  selector: 'app-footer-component',
  template: '<router-outlet></router-outlet>'
})
export class PageComponent { }

