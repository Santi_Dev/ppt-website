import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import AppRoutes from '../../constants/routes';
import { PageComponent } from './pages.component';
import GameComponent from '../pages/game/game.component';
import RoundComponent from '../pages/round/round.component';
import { WinComponent } from '../pages/win/win.component';

const routes: Routes = [
    {
        path: '',
        component: PageComponent,
        children: [
            {
                path: `${AppRoutes.GAME_ROUTE}/new`,
                component: GameComponent
            },
            {
                path: `${AppRoutes.GAME_ROUTE}/:id/rounds`,
                component: RoundComponent
            },
            {
                path: `${AppRoutes.GAME_ROUTE}/:id/win`,
                component: WinComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PagesRoutingModule { }
