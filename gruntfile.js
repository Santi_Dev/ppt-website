module.exports = (grunt) => {
    // Project configuration.
    grunt.initConfig({
        clean: {
            default: ["build/**/*", "dist/**/*"]
        },
        copy: {
            default: {
                files: [
                    {
                        expand: true,
                        src: "server.js",
                        dest: "dist/",
                    },
                    {
                        src: "package-server.json",
                        dest: "dist/package.json",
                    }
                ],
            }
        },
        exec: {
            prod: 'ng build --configuration=production'
        }
    });

    // Load plugins for Tasks.
    grunt.loadNpmTasks("grunt-contrib-clean");
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks('grunt-exec');

    grunt.registerTask("build-prd", ["clean", "exec:prod", "copy"]);
};
